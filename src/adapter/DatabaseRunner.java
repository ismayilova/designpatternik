package adapter;

public class DatabaseRunner {

    public static void main(String[] args) {
        Database database = new AdapterJava2Database();
        database.insert();
        database.update();
        database.select();
        database.remove();
    }
}
