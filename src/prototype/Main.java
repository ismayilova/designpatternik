package prototype;

public class Main {
    public static void main(String[] args) {
        Project master = new Project(1,"SuperProject", "SourceCoden sourceCode = new SourceCoden()");

        //1sposob

       Project masterClone  =(Project) master.copy();

      // master.setId(3);

        System.out.println(master);
      //  System.out.println(masterClone);
//2 sposob s projectFactory

        ProjectFactory projectFactory = new ProjectFactory(master);

        Project masterClone2 = projectFactory.cloneProject();

        System.out.println(masterClone2);


    }
}
