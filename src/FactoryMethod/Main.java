package FactoryMethod;

public class Main {

    public static void main(String[] args) {
//Fabric method
           DeveloperFactory developerFactory = createDeveloperFactory("c++");
           Developer developer = developerFactory.createDeveloper();
           developer.writeCode();


        //Abstracr FabricMethod
    }

    static DeveloperFactory createDeveloperFactory(String lang){
        if(lang.equalsIgnoreCase("java")){
            return  new JavaDeveloperFactory();
        }else if(lang.equalsIgnoreCase("c++")){
            return new CppDeveloperFactory();
        }else{
            throw new RuntimeException(lang + " unknow lang");
        }
    }
}

