package AbstractFactoryMethod;

public interface Developer {
    void writeCode();
}
