package AbstractFactoryMethod;

public interface ProjectManager {
    void manageProject();
}
