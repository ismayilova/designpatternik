package AbstractFactoryMethod;

import AbstractFactoryMethod.banking.BankingTeamFactory;

public class Main {
    public static void main(String[] args) {
        //New banking system

        ProjectTeamCreateFactory projectTeamCreateFactory = new BankingTeamFactory();
        Developer developer = projectTeamCreateFactory.getDeveloper();
        Tester tester = projectTeamCreateFactory.getTester();
        ProjectManager projectManager = projectTeamCreateFactory.getProjectManager();

        System.out.println("Creating      ===================");
        developer.writeCode();
        tester.testCode();
        projectManager.manageProject();


    }
}
