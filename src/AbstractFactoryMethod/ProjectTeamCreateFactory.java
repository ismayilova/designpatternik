package AbstractFactoryMethod;

public interface ProjectTeamCreateFactory {
    Developer getDeveloper();
    Tester getTester();
    ProjectManager getProjectManager();
}
