package AbstractFactoryMethod;

public interface Tester {
    void testCode();
}
