package AbstractFactoryMethod.banking;

import AbstractFactoryMethod.Developer;
import AbstractFactoryMethod.ProjectManager;
import AbstractFactoryMethod.ProjectTeamCreateFactory;
import AbstractFactoryMethod.Tester;

public class BankingTeamFactory implements ProjectTeamCreateFactory {
    @Override
    public Developer getDeveloper() {
        return new JavaDeveloper();
    }

    @Override
    public Tester getTester() {
        return new QATester();
    }

    @Override
    public ProjectManager getProjectManager() {
        return new BankingPM();
    }
}
