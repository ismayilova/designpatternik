package AbstractFactoryMethod.banking;

import AbstractFactoryMethod.Tester;

public class QATester implements Tester {
    @Override
    public void testCode() {
        System.out.println("QA test banking code...");
    }
}
