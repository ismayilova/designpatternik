package AbstractFactoryMethod.website;

import AbstractFactoryMethod.Developer;
import AbstractFactoryMethod.ProjectManager;
import AbstractFactoryMethod.ProjectTeamCreateFactory;
import AbstractFactoryMethod.Tester;

public class WebsiteTeamFactory implements ProjectTeamCreateFactory {
    @Override
    public Developer getDeveloper() {
        return new PhpDeveloper();
    }

    @Override
    public Tester getTester() {
        return new ManualTester();
    }

    @Override
    public ProjectManager getProjectManager() {
        return new WebsitePM();
    }
}
