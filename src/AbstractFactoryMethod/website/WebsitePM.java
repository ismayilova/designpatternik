package AbstractFactoryMethod.website;

import AbstractFactoryMethod.ProjectManager;

public class WebsitePM  implements ProjectManager {
    @Override
    public void manageProject() {
        System.out.println( "Website PM manages project. ..");
    }
}
