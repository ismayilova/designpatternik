package AbstractFactoryMethod.website;

import AbstractFactoryMethod.Tester;

public class ManualTester implements Tester {
    @Override
    public void testCode() {
        System.out.println("Manual tester test website project...");
    }
}
