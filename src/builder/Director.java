package builder;

public class Director {
    WebsiteBuilder builder;

    void setBuilder(WebsiteBuilder websiteBuilder){
        builder = websiteBuilder;
    }

    Website createWebsite(){
        builder.createWebsite();
        builder.buildCms();
        builder.buildName();
        builder.buildPrice();

        Website website = builder.getWebsite();
        return website;
    }
}
