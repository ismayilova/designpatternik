public class Response {
    private Object data;


    public Response(Object data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "Response{" +
                "data=" + data +
                '}';
    }
}
